package com.example.DirectoryPicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class LauncherActivity extends Activity {
    private Context mContext = this;
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.launch);

        Button button       = (Button) findViewById(R.id.button_dir);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,DirectoryPicker.class);
                startActivityForResult(intent,0);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK){
            String result = data.getStringExtra("dirPath");
            TextView textView   = (TextView) findViewById(R.id.text_result);
            textView.setText(result);
            Log.v("TAG", result);
        }
    }
}