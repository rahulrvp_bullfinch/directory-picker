package com.example.DirectoryPicker;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import java.io.File;
import java.util.ArrayList;

public class DirectoryPicker extends Activity {
    private String LOG_TAG = "DirectoryPicker";
    private Context mContext = this;
    private ArrayList<File> mParentList;
    private File mParent;
    private DirectoryListAdapter dirListAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.directory_list);

        mParentList = new ArrayList<File>();

        dirListAdapter = new DirectoryListAdapter(mContext);
        ListView dirListView = (ListView) findViewById(R.id.list_dir);
        dirListView.setAdapter(dirListAdapter);

        final File rootDir = new File("/mnt/");
        mParent = rootDir;
        new Thread(new Runnable() {
            @Override
            public void run() {
                dirListAdapter.setDirList(scanDirectory(rootDir));
            }
        }).start();

        dirListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                mParentList.add(mParent);
                mParent = (File) dirListAdapter.getItem(position);
                dirListAdapter.setDirList(scanDirectory(mParent));
                dirListAdapter.notifyDataSetChanged();
                Log.v(LOG_TAG,"position" + position);
            }
        });

        Button selectButton = (Button) findViewById(R.id.button_select);
        selectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.putExtra("dirPath", mParent.getAbsolutePath());
                if (getParent() == null){
                    setResult(Activity.RESULT_OK,intent);
                } else {
                    getParent().setResult(Activity.RESULT_OK,intent);
                }
                finish();
            }
        });
    }

    @Override
    public void onBackPressed(){
        mParent = getParentDir();
        if(mParent != null){
            dirListAdapter.setDirList(scanDirectory(mParent));
        } else {
            finish();
        }
    }

    private File getParentDir(){
        try{
            File parent = mParentList.get(mParentList.size()-1);
            mParentList.remove(mParentList.size()-1);
            return parent;
        } catch (ArrayIndexOutOfBoundsException e){
            return null;
        }
    }

    private ArrayList<File> scanDirectory(File dir){
        ArrayList<File> dirList = new ArrayList<File>();
        if (dir.isDirectory()){
            File[] listOfContents = dir.listFiles();
            if (listOfContents != null){
                for (File item: listOfContents){
                    if(item.isDirectory()) dirList.add(item);
                }
            }
        }
        return dirList;
    }
}
