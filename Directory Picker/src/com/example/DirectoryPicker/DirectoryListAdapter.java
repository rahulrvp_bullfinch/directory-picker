package com.example.DirectoryPicker;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

public class DirectoryListAdapter extends BaseAdapter {
    private ArrayList<File> mDirList = new ArrayList<File>();
    private Context mContext;

    public DirectoryListAdapter(Context context){
        mContext = context;
    }

    public void setDirList(ArrayList<File> dirList){
        this.mDirList = dirList;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mDirList.size();
    }

    @Override
    public Object getItem(int position) {
        return mDirList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null){
            convertView = LayoutInflater.from(mContext).inflate(R.layout.cell, null);
            ViewHolder viewHolder = new ViewHolder();
            viewHolder.mDirectoryName = (TextView) convertView.findViewById(R.id.txt_dir_name);
            convertView.setTag(viewHolder);
        }
        ((ViewHolder) convertView.getTag()).mDirectoryName.setText(mDirList.get(position).getName());
        return convertView;
    }

    class ViewHolder{
        TextView mDirectoryName;
    }
}
